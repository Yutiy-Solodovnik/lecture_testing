﻿using System;

namespace lecture_EF.DAL.Entites
{
    public class ProjectInfo
    { 
        public Project Project { get; set; }
        public MyTask LongestTask { get; set; }
        public MyTask ShortestTask { get; set; }
        public int AllUsersOnProject { get; set; }
    }
}
