﻿using lecture_EF.DAL.EF;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using System;

namespace lecture_EF.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private IRepository<MyTask> _myTaskRepository;
        private IRepository<Project> _projectRepository;
        private IRepository<Team> _teamRepository;
        private IRepository<User> _userRepository;
        private ProjectsContext db;

        public UnitOfWork(ProjectsContext options, IRepository<Project> projectRepository, IRepository<MyTask> myTaskRepository, IRepository<Team> teamRepository, IRepository<User> userRepository)
        {
            db = options;
            _projectRepository = projectRepository;
            _myTaskRepository = myTaskRepository;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
        }

        public IRepository<MyTask> Tasks
        {
            get
            {
                if (_myTaskRepository == null)
                    _myTaskRepository = new MyTaskRepositry(db);
                return _myTaskRepository;
            }
        }
        public IRepository<Project> Projects
        {
            get
            {
                if (_projectRepository == null)
                    _projectRepository = new ProjectRepository(db);
                return _projectRepository;
            }
        }
        public IRepository<Team> Teams
        {
            get
            {
                if (_teamRepository == null)
                    _teamRepository = new TeamRepository(db);
                return _teamRepository;
            }
        }
        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(db);
                return _userRepository;
            }
        }
        public void Save()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
