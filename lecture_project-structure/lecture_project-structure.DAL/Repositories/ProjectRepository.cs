﻿using lecture_EF.DAL.EF;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace lecture_EF.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private ProjectsContext db;
        public ProjectRepository(ProjectsContext context)
        {
            db = context;
        }
        public void Create(Project item)
        {
            db.Projects.Add(item);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Project project = db.Projects.Find(id);
            if (project != null)
            {
                db.Projects.Remove(project);
                db.SaveChanges();
            }
        }

        public Project Read(int id)
        {
            return db.Projects.Find(id);
        }

        public IEnumerable<Project> ReadAll()
        {
            return db.Projects.ToList();
        }

        public void Update(Project item)
        {
            Project project = Read(item.Id);
            if (project != null)
            {
                project.Name = item.Name;
                project.Team = item.Team;
                project.TeamId = item.TeamId;
                project.Tasks = item.Tasks;
                project.Author = item.Author;
                project.AuthorId = item.AuthorId;
                project.Description = item.Description;
                project.Deadline = item.Deadline;
                project.CreatedAt = item.CreatedAt;
                db.Entry(project).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
