﻿using lecture_EF.DAL.EF;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace lecture_EF.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private ProjectsContext db;
        public TeamRepository(ProjectsContext context)
        {
            db = context;
        }
        public void Create(Team item)
        {
            db.Teams.Add(item);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Team task = db.Teams.Find(id);
            if (task != null)
            {
                db.Teams.Remove(task);
                db.SaveChanges();
            }   
        }

        public Team Read(int id)
        {
            return db.Teams.Find(id);
        }

        public IEnumerable<Team> ReadAll()
        {
            return db.Teams.ToList();
        }

        public void Update(Team item)
        {
            Team team = Read(item.Id);
            if (team != null)
            {
                team.Name = item.Name;
                team.CreatedAt = item.CreatedAt;
                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
