﻿using lecture_EF.BLL.DTO;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace WebApi.Projects.IntegrationTests
{
    public class UserControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _factory;

        public UserControllerIntegrationTests(CustomWebApplicationFactory factory)
        {
            _factory = factory;
        }

        [Fact]
        public  async Task Delete_User_By_Id()
        {
            UserDTO userDTO = new UserDTO()
            {   
                Id =300,
                FirstName = "Ivan",
                LastName ="Petrov",
                BirthDay = DateTime.Parse("1978-06-15T18:48:06.062331") 
            };
            string json = JsonConvert.SerializeObject(userDTO);
            await _factory.Client.PostAsync("api/Users", new StringContent(json));
            var response = await _factory.Client.DeleteAsync("api/Users/300");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory]
        [InlineData("100")]
        [InlineData("bad")]
        public async Task Delete_User_By_Wrong_Id(string id)
        {
            var response = await _factory.Client.DeleteAsync($"api/Users/{id}");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get_Finished_Tasks_By_User_Right_Id()
        {
            UserDTO userDTO = new UserDTO()
            {
                Id = 300,
                FirstName = "Ivan",
                LastName = "Petrov",
                BirthDay = DateTime.Parse("1978-06-15T18:48:06.062331")
            };
            string json = JsonConvert.SerializeObject(userDTO);
            await _factory.Client.PostAsync("api/Users", new StringContent(json));
            var response = await _factory.Client.GetAsync("api/Users/tasksByUser/300");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task Get_Finished_Tasks_By_User_Wrong_Id()
        {
            var response = await _factory.Client.GetAsync("api/Users/tasksByUser/bad");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Theory]
        [InlineData("100")]
        [InlineData("1")]
        public async Task Get_Unfinished_Tasks_By_User_Right_Id(string id)
        {
            var response = await _factory.Client.GetAsync($"api/Users/unfinishedTasksByUser/{id}");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory]
        [InlineData("{}")]
        [InlineData("bad")]
        public async Task Get_Unfinished_Tasks_By_User_Wrong_Id(string id)
        {
            var response = await _factory.Client.GetAsync($"api/Users/unfinishedTasksByUser/{id}");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }   
}
