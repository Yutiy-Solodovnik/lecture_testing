﻿using lecture_EF.DAL.EF;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net.Http;

namespace WebApi.Projects.IntegrationTests
{
    public class CustomWebApplicationFactory
    {
        public TestServer TestServer { get; }
        public ProjectsContext DbContext { get; }
        public HttpClient Client { get; }

        public CustomWebApplicationFactory()
        {
            var builder = new WebHostBuilder()
                .UseEnvironment("Testing")
                .UseStartup<Startup>();

            TestServer = new TestServer(builder);
            Client = TestServer.CreateClient();
            Client.BaseAddress = new Uri("https://localhost:44300/");
            DbContext = TestServer.Host.Services.GetService<ProjectsContext>();
        }
    }
}
