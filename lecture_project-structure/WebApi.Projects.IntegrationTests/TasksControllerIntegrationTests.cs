﻿using lecture_EF.BLL.DTO;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace WebApi.Projects.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _factory;

        public TasksControllerIntegrationTests(CustomWebApplicationFactory factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task Delete_Task_By_Id()
        {
            MyTaskDTO taskDTO = new MyTaskDTO()
            {
                Id = 300,
                Name = "test",
                CreatedAt = DateTime.Parse("2019-06-15T18:48:06.062331"),
                FinishedAt = DateTime.Parse("2020-06-15T18:48:06.062331"),
                Description = "Lala",
                PerformerId = 1
            };
            string json = JsonConvert.SerializeObject(taskDTO);
            await _factory.Client.PostAsync("api/Tasks", new StringContent(json));
            var response = await _factory.Client.DeleteAsync($"api/Tasks/300");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory]
        [InlineData("{}")]
        [InlineData("bad")]
        public async Task Delete_Task_By_Wrong_Id(string id)
        {
            var response = await _factory.Client.DeleteAsync($"api/Tasks/{id}");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}