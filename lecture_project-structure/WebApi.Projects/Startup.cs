using lecture_EF.BLL.Interfaces;
using lecture_EF.BLL.Services;
using lecture_EF.DAL.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using lecture_EF.DAL.Interfaces;
using lecture_EF.DAL.Repositories;
using lecture_EF.DAL.Entites;

namespace WebApi.Projects
{
    public class Startup
    {
        private readonly IWebHostEnvironment _currentEnvironment;

        public Startup(IConfiguration configuration, IWebHostEnvironment currentEnvironment)
        {
            Configuration = configuration;
            _currentEnvironment = currentEnvironment;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IRepository<Project>, ProjectRepository>();
            services.AddTransient<IRepository<MyTask>, MyTaskRepositry>();
            services.AddTransient<IRepository<Team>, TeamRepository>();
            services.AddTransient<IRepository<User>, UserRepository>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IMyTaskService, MyTaskService>();
            services.AddTransient<ITeamService, TeamService>();
            services.AddTransient<IUserService, UserService>();

            services.AddAutoMapper(typeof(Startup));

            AddDb(services);

            ConfigureDependencies(services);

            services.AddControllers();  
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebApi.Projects", Version = "v1" });
            });
        }

        private void AddDb(IServiceCollection services)
        {
            if (_currentEnvironment.IsEnvironment("Testing"))
            {
                services.AddDbContextPool<ProjectsContext>(options =>
                    options.UseInMemoryDatabase("TestingDB"));
            }
            else
            {
                services.AddDbContextPool<ProjectsContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("ProjectsDatabaseConnection")));
            }
        }

        public virtual void ConfigureDependencies(IServiceCollection services)
        {

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
