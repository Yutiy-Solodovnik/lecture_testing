﻿using AutoMapper;
using lecture_EF.BLL.DTO;
using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace WebApi.Users.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class UsersController : Controller
    {
        private IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        public ActionResult<UserDTO> CreateUser()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(todoJson.Result);
                User user = _mapper.Map<User>(userDTO);
                _userService.CreateUser(user);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetUser(string id)
        {
            User user = _userService.GetUser(id);
            UserDTO userDTO = _mapper.Map<UserDTO>(user);
            return Ok(userDTO);
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetALLUsers()
        {
            IEnumerable<User> users = _userService.GetAllUsers();
            IEnumerable<UserDTO> usersDTO = _mapper.Map<IEnumerable<UserDTO>>(users);
            return Ok(usersDTO);
        }

        [HttpGet("unfinishedTasksByUser/{id}")]
        public ActionResult<List<MyTaskDTO>> GetUnfinishedTasksByUser(string id)
        {
            try
            {
                List<MyTask> tasks = _userService.GetUnfinishedTasksByUser(id);
                List<MyTaskDTO> tasksDTO = _mapper.Map<List<MyTaskDTO>>(tasks);
                return Ok(tasksDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("tasksInProjectOfUser/{id}")]
        public ActionResult<Dictionary<string, int>> GetTasksInProject(string id)
        {
            Dictionary<string, int> tasks = _userService.GetTasksInProject(id);
            return Ok(tasks);
        }

        [HttpGet("finishedTasksByUser/{id}")]
        public ActionResult<IEnumerable<MyTaskInfoDTO>> GetFinishedTasksByUser(string id)
        {
            try
            {
                IEnumerable<MyTaskInfo> myTasksInfo = _userService.GetFinishedTasksByUser(id);
                IEnumerable<MyTaskInfoDTO> myTasksInfoDTO = _mapper.Map<IEnumerable<MyTaskInfoDTO>>(myTasksInfo);
                return Ok(myTasksInfoDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("tasksByUser/{id}")]
        public ActionResult<List<MyTaskDTO>> GetTasksByUser(string id)
        {
            try
            {
                List<MyTask> tasks = _userService.GetTasksByUser(id);
                List<MyTaskDTO> tasksDTO = _mapper.Map<List<MyTaskDTO>>(tasks);
                return Ok(tasksDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("usersByName")]
        public ActionResult<List<UserDTO>> GetUsersByName()
        {
            List<User> users = _userService.GetUsersByName();
            List<UserDTO> usersDTO = _mapper.Map<List<UserDTO>>(users);
            return Ok(usersDTO);
        }

        [HttpGet("userInfo/{id}")]
        public ActionResult<UserInfoDto> GetUserInfo(string id)
        {
            UserInfo users = _userService.GetUserInfo(id);
            UserInfoDto usersDTO = _mapper.Map<UserInfoDto>(users);
            return Ok(usersDTO);
        }

        [HttpDelete("{id}")]
        public ActionResult<string> DeleteUser(string id)
        {
            try
            {
                _userService.DeleteUser(id);
                return Ok("Deleted");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<string> UpdateUser()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(todoJson.Result);
                User user = _mapper.Map<User>(userDTO);
                _userService.UpdateUser(user);
                return Ok("Updated");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("addUserToTeam")]
        public ActionResult<string> AddUserToTeam()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(todoJson.Result);
                User user = _mapper.Map<User>(userDTO);
                _userService.AddUserToTeam(user);
                return Ok("Added");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
