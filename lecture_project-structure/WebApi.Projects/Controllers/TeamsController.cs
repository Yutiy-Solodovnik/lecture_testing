﻿using AutoMapper;
using lecture_EF.BLL.DTO;
using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Teams.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TeamsController : Controller
    {
        private ITeamService _teamService;
        private readonly IMapper _mapper;

        public TeamsController(IMapper mapper, ITeamService teamService)
        {
            _mapper = mapper;
            _teamService = teamService;
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetTeam(string id)
        {
            Team team = _teamService.GetTeam(id);
            TeamDTO teamDTO = _mapper.Map<TeamDTO>(team);
            return Ok(teamDTO);
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetALLTeams()
        {
            IEnumerable<Team> teams = _teamService.GetAllTeams();
            IEnumerable<TeamDTO> teamsDTO = _mapper.Map<IEnumerable<TeamDTO>>(teams);
            return Ok(teamsDTO);
        }

        [HttpGet("usersFromTeams")]
        public ActionResult<IEnumerable<IGrouping<int?, UserDTO>>> GetUsersFromTeam()
        {
            IEnumerable<IGrouping<int?, User>> users = _teamService.GetUsersFromTeam();
            return Ok(users);
        }

        [HttpPost]
        public ActionResult<TeamDTO> CreateTeam()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                TeamDTO teamDTO = JsonConvert.DeserializeObject<TeamDTO>(todoJson.Result);
                Team team = _mapper.Map<Team>(teamDTO);
                _teamService.CreateTeam(team);
                return Ok(team);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<string> DeleteTeam(string id)
        {
            try
            {
                _teamService.DeleteTeam(id);
                return Ok("Deleted");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public ActionResult<string> UpdateTeam()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                TeamDTO teamDTO = JsonConvert.DeserializeObject<TeamDTO>(todoJson.Result);
                Team team = _mapper.Map<Team>(teamDTO);
                _teamService.UpdateTeam(team);
                return Ok("Updated");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
