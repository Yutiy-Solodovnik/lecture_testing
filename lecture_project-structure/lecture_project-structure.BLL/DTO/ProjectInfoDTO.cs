﻿using System;

namespace lecture_EF.BLL.DTO
{
    public class ProjectInfoDTO
    {
        public int ProjectId { get; set; }
        public int LongestTaskId { get; set; }
        public int ShortestTaskId { get; set; }
        public int AllUsersOnProject { get; set; }
    }
}
