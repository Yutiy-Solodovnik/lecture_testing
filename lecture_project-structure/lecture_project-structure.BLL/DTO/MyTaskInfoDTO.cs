﻿using System;

namespace lecture_EF.BLL.DTO
{
    public class MyTaskInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
