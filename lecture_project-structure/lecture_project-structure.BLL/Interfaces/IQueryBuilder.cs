﻿using lecture_EF.DAL.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Interfaces
{
    public interface IQueryBuilder
    {
        public void UpdateInformation();

        public void GetHierarchy();

        public Dictionary<string, int> GetTasksInProject(int Id);

        public List<MyTask> GetTasksByUser(int Id);
        public List<MyTaskInfo> GetFinishedTasksByUser(int Id);

        public IEnumerable<IGrouping<int?, User>> GetUsersFromTeam();

        public List<User> GetUsersByName();

        public bool IsTeamExist(int id);
        public UserInfo GetUserInfo(int Id);

        public List<ProjectInfo> GetProjectInfo();
        List<MyTask> GetUnfinishedTasksByUser(int id);
    }
}
