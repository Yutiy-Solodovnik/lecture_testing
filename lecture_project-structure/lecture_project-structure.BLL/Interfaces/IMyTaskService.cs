﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;

namespace lecture_EF.BLL.Interfaces
{
    public interface IMyTaskService
    {
        void CreateTask(MyTask task);
        IEnumerable<MyTask> GetAllTasks();
        MyTask GetTask(string id);
        void UpdateTask(MyTask task);
        void DeleteTask(string id);
        void ChangeTaskState(MyTask task);
        void Dispose();
    }
}
