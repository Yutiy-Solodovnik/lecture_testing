﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;

namespace lecture_EF.BLL.Interfaces
{
    public interface IUserService
    {
        void CreateUser(User team);
        IEnumerable<User> GetAllUsers();
        User GetUser(string id);
        void UpdateUser(User user);
        void DeleteUser(string id);
        IEnumerable<MyTaskInfo> GetFinishedTasksByUser(string id);
        List<MyTask> GetTasksByUser(string id);
        Dictionary<string, int> GetTasksInProject(string id);
        List<User> GetUsersByName();
        UserInfo GetUserInfo(string id);
        void AddUserToTeam(User user);
        void Dispose();
        List<MyTask> GetUnfinishedTasksByUser(string id);
    }
}
