﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace lecture_EF.BLL.Services
{
    public class MyTaskService : IMyTaskService
    {
        IUnitOfWork Database { get; set; }

        public MyTaskService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }
        public void CreateTask(MyTask task)
        {
            if (task == null)
                throw new NullReferenceException("Task is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(task);
            string errors = null;
            
            if (!Validator.TryValidateObject(task, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                Database.Tasks.Create(task);
            }
        }

        public void DeleteTask(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id))
                Database.Tasks.Delete(_id);
            else
                throw new ArgumentException("Invalid Id");            
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<MyTask> GetAllTasks()
        {
            return Database.Tasks.ReadAll();
        }

        public MyTask GetTask(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id))
                return Database.Tasks.Read(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public void ChangeTaskState(MyTask task)
        {
            if (task == null)
                throw new NullReferenceException("Task is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(task);
            string errors = null;

            if (!Validator.TryValidateObject(task, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else if(task.FinishedAt != null)
            {
                throw new ArgumentException("Task is finished");
            }
            else
            {
                task.FinishedAt = DateTime.Now;
                Database.Tasks.Update(task);
            }
        }

        public void UpdateTask(MyTask task)
        {
            if (task == null)
                throw new NullReferenceException("Task is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(task);
            string errors = null;
            
            if (!Validator.TryValidateObject(task, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                Database.Tasks.Update(task);
            }
        }
    }
}
