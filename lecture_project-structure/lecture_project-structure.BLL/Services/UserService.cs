﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace lecture_EF.BLL.Services
{
    public class UserService : IUserService
    {
        public IQueryBuilder QueryBuilder { get; set; }
        IUnitOfWork Database { get; set; }

        public UserService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
            QueryBuilder = new QueryBuilder(unitOfWork);
        }

        public void CreateUser(User user)
        {
            if (user == null)
                throw new NullReferenceException("User is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(user);
            string errors = null;
            
            if (!Validator.TryValidateObject(user, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                Database.Users.Create(user);
            }
        }

        public void DeleteUser(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id) && Database.Users.Read(_id) != null)
                Database.Users.Delete(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<User> GetAllUsers()
        {
            return Database.Users.ReadAll();
        }

        public void AddUserToTeam(User user)
        {
            if (user == null)
                throw new NullReferenceException("User is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(user);
            string errors = null;

            if (!Validator.TryValidateObject(user, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else if ((user.TeamId == null))
            {
                throw new ArgumentException("Team is not exist");
            }
            else
            {
                Database.Users.Update(user);
            }
        }
        public User GetUser(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id))
                return Database.Users.Read(_id);
            else
                throw new ArgumentException("Invalid Id");
        }


        public Dictionary<string, int> GetTasksInProject(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id))
                return QueryBuilder.GetTasksInProject(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public List<MyTask> GetTasksByUser(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id) && id != "")
                return QueryBuilder.GetTasksByUser(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public IEnumerable<MyTaskInfo> GetFinishedTasksByUser(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id))
                return QueryBuilder.GetFinishedTasksByUser(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public List<User> GetUsersByName()
        {
            return QueryBuilder.GetUsersByName();
        }

        public UserInfo GetUserInfo(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id))
                return QueryBuilder.GetUserInfo(_id);
            else
                throw new ArgumentException("Invalid Id");
        }
        public void UpdateUser(User user)
        {
            if (user == null)
                throw new NullReferenceException("User is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(user);
            string errors = null;
            
            if (!Validator.TryValidateObject(user, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                Database.Users.Update(user);
            }
        }

        public List<MyTask> GetUnfinishedTasksByUser(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id))
                return QueryBuilder.GetUnfinishedTasksByUser(_id);
            else
                throw new ArgumentException("Invalid Iddd");
        }
    }
}
