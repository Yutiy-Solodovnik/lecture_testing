﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace lecture_EF.BLL.Services
{
    public class TeamService : ITeamService
    {
        public IQueryBuilder QueryBuilder { get; set; }
        IUnitOfWork Database { get; set; }

        public TeamService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
            QueryBuilder = new QueryBuilder(unitOfWork);
        }

        public void CreateTeam(Team team)
        {
            if (team == null)
                throw new NullReferenceException("Team is empty");
            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(team);
            string errors = null;
            
            if (!Validator.TryValidateObject(team, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                Database.Teams.Create(team);
            }
        }

        public void DeleteTeam(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id) && Database.Teams.Read(_id) != null)
                Database.Teams.Delete(_id);
            else
                throw new ArgumentException("Invalid Id");
        }
        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<Team> GetAllTeams()
        {
            return Database.Teams.ReadAll();
        }

        public Team GetTeam(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id))
                return Database.Teams.Read(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public IEnumerable<IGrouping<int?, User>> GetUsersFromTeam()
        {
            return QueryBuilder.GetUsersFromTeam();
        }

        public void UpdateTeam(Team team)
        {
            if (team == null)
                throw new NullReferenceException("Team is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(team);
            string errors = null;
            
            if (!Validator.TryValidateObject(team, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                Database.Teams.Update(team);
            }
        }
    }
}
