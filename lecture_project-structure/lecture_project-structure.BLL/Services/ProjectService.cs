﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace lecture_EF.BLL.Services
{
    public class ProjectService : IProjectService
    {
        public IQueryBuilder QueryBuilder { get; set; }
        IUnitOfWork Database { get; set; }

        public ProjectService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
            QueryBuilder = new QueryBuilder(unitOfWork);
        }
        public void CreateProject(Project project)
        {
            if (project == null)
                throw new NullReferenceException("Project is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(project);
            string errors = null;
            
            if (!Validator.TryValidateObject(project, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                Database.Projects.Create(project);
            }
        }

        public void DeleteProject(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id) && Database.Projects.Read(_id) != null)
                Database.Projects.Delete(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public IEnumerable<Project> GetAllProjects()
        {
            return Database.Projects.ReadAll();
        }

        public Project GetProject(string id)
        {
            int _id;
            if (Int32.TryParse(id, out _id))
                return Database.Projects.Read(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public  List<ProjectInfo> GetProjectInfo()
        {
            return QueryBuilder.GetProjectInfo();
        }

        public void UpdateProject(Project project)
        {
            if (project == null)
                throw new NullReferenceException("Project is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(project);
            string errors = null;
            
            if (!Validator.TryValidateObject(project, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                Database.Projects.Update(project);
            }
        }
    }
}
