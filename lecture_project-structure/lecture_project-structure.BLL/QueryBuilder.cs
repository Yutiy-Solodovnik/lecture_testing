﻿using System;
using lecture_EF.DAL.Entites;
using System.Collections.Generic;
using System.Linq;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;

namespace lecture_EF.BLL
{
    public class QueryBuilder : IQueryBuilder
    {
        private List<Project> _projectsList;
        private List<User> _usersList;
        private List<Team> _teamsList;
        private List<MyTask> _tasksList;
        private List<Project> _hierarchy;
        private IUnitOfWork _unitOfWork;

        public QueryBuilder(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            GetHierarchy();
        }
        public  void UpdateInformation()
        {
            _projectsList = _unitOfWork.Projects.ReadAll().ToList();
            _usersList = _unitOfWork.Users.ReadAll().ToList();
            _teamsList = _unitOfWork.Teams.ReadAll().ToList();
            _tasksList = _unitOfWork.Tasks.ReadAll().ToList();
        }

        public  void GetHierarchy()
        {
            if (_hierarchy == null)
                UpdateInformation();
            _hierarchy = _projectsList.GroupJoin(_tasksList, p => p.Id, t => t.ProjectId,
                (p, t) => new Project()
                {
                    Id = p.Id,
                    AuthorId = p.AuthorId,
                    TeamId = p.TeamId,
                    Name = p.Name,
                    Description = p.Description,
                    Deadline = p.Deadline,
                    CreatedAt = p.CreatedAt,
                    Tasks = t.GroupJoin(_usersList, t => t.PerformerId, u => u.Id,
                        (t, u) => new MyTask()
                        {
                            Id = t.Id,
                            ProjectId = t.ProjectId,
                            PerformerId = t.PerformerId,
                            Performer = _usersList.Where(u => u.Id == t.PerformerId).FirstOrDefault(),
                            Name = t.Name,
                            Description = t.Description,
                            State = t.State,
                            CreatedAt = t.CreatedAt,
                            FinishedAt = t.FinishedAt
                        }
                        ).ToList(),
                    Team = _teamsList.Where(t => t.Id == p.TeamId).FirstOrDefault(),
                    Author = _usersList.Where(u => u.Id == p.AuthorId).First()
                })
                .ToList();
        }

        public  Dictionary<string, int> GetTasksInProject(int Id)
        {
            return _hierarchy
                .Where(u => u.AuthorId == Id)
                .Select(t => new { projectName = t.Name, taskCunt = t.Tasks.Count() })
                .ToDictionary(x => x.projectName, x => x.taskCunt);
        }

        public  List<MyTask> GetTasksByUser(int Id)
        {
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .Where(t => t.PerformerId == Id && t.Name.Length < 45)
               .ToList();
        }
        public  List<MyTaskInfo> GetFinishedTasksByUser(int Id)
        {
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .Where(t => t.PerformerId == Id && t.FinishedAt != null)
               .Select(t => new MyTaskInfo { Id = t.Id, Name = t.Name }).ToList();
        }

        public  IEnumerable<IGrouping<int?, User>> GetUsersFromTeam()
        {
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .Select(u => u.Performer)
               .Where(u => (DateTime.Now.Year - u.BirthDay.Year) > 10)
               .Distinct().OrderBy(y => y.RegisteredAt)
               .GroupBy(t => t.TeamId);
        }

        public  List<User> GetUsersByName()
        {
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .OrderByDescending(t => t.Name)
               .Select(u => u.Performer)
               .Distinct()
               .OrderBy(y => y.FirstName)
               .ToList();
        }
        
        public bool IsTeamExist(int id)
        {
            return _teamsList.Where(t => t.Id == id).FirstOrDefault() == null ? false : true;
        }
        public  UserInfo GetUserInfo(int Id)
        {
            return _hierarchy
               .Select(p => new UserInfo
               {
                   User = _hierarchy
                           .SelectMany(t => t.Tasks)
                           .Select(u => u.Performer)
                           .Where(u => u.Id == Id)
                           .Select(u => u)
                           .First(),
                   LastProject = _hierarchy
                            .Where(u => u.AuthorId == Id)
                            .OrderBy(x => x.CreatedAt)
                            .First(),
                   AllTasks = _hierarchy
                            .Where(u => u.AuthorId == Id)
                            .OrderBy(x => x.CreatedAt)
                            .First().Tasks
                            .Count(),
                   AllUnfinishedTasks = _hierarchy
                           .SelectMany(t => t.Tasks)
                           .Where(t => t.PerformerId == Id && t.FinishedAt == null)
                           .Count(),
                   LongestTask = _hierarchy
                           .SelectMany(t => t.Tasks)
                           .Where(t => t.PerformerId == Id)
                           .OrderByDescending(x => (x.FinishedAt ?? DateTime.Now) - x.CreatedAt)
                           .First()
               }).ToList()[0];
        }

        public  List<ProjectInfo> GetProjectInfo()
        {
            return _hierarchy
               .Select(p => new ProjectInfo
               {
                   Project = p,
                   LongestTask = p.Tasks
                       .OrderByDescending(x => x.Description.Length)
                       .FirstOrDefault(),
                   ShortestTask = p.Tasks
                        .OrderBy(x => x.Name.Length)
                        .FirstOrDefault(),
                   AllUsersOnProject = p.Tasks
                        .Where(t => p.Description.Length > 20 || p.Tasks.Count() < 3)
                        .Select(t => t.Performer)
                        .Count()
               }).ToList();
        }

        public List<MyTask> GetUnfinishedTasksByUser(int id)
        {
            return _tasksList.Where(t => t.PerformerId == id && t.FinishedAt == null).ToList();
        }
    }
}
