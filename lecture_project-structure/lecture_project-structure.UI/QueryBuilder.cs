﻿using lecture_EF.DAL.Entites;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lecture_EF.UI
{
    public static class QueryBuilder
    {
        private static List<Project> _projectsList;
        private static List<User> _usersList;
        private static List<Team> _teamsList;
        private static List<MyTask> _tasksList;
        private static List<Project> _hierarchy;

        private static void UpdateInformation()
        {
            _projectsList = JsonReader.GetEntitiesList<Project>();
            _usersList = JsonReader.GetEntitiesList<User>();
            _teamsList = JsonReader.GetEntitiesList<Team>();
            _tasksList = JsonReader.GetEntitiesList<MyTask>();
        }

        public static void GetHierarchy()
        {
            UpdateInformation();
            _hierarchy = _projectsList.GroupJoin(_tasksList, p => p.Id, t => t.ProjectId,
               (p, t) => new Project()
               {
                   Id = p.Id,
                   AuthorId = p.AuthorId,
                   TeamId = p.TeamId,
                   Name = p.Name,
                   Description = p.Description,
                   Deadline = p.Deadline,
                   CreatedAt = p.CreatedAt,
                   Tasks = t.GroupJoin(_usersList, t => t.PerformerId, u => u.Id,
                       (t, u) => new MyTask()
                       {
                           Id = t.Id,
                           ProjectId = t.ProjectId,
                           PerformerId = t.PerformerId,
                           Performer = _usersList.Where(u => u.Id == t.PerformerId).FirstOrDefault(),
                           Name = t.Name,
                           Description = t.Description,
                           State = t.State,
                           CreatedAt = t.CreatedAt,
                           FinishedAt = t.FinishedAt
                       }
                       ).ToList(),
                   Team = _teamsList.Where(t => t.Id == p.TeamId).FirstOrDefault(),
                   Author = _usersList.Where(u => u.Id == p.AuthorId).First()
               })
               .ToList();
        }

        public static Dictionary<string, int> GetTasksInProject(int Id)
        {
            return _hierarchy
                .Where(u => u.AuthorId == Id)
                .Select(t => new { projectName = t.Name, taskCunt = t.Tasks.Count() })
                .ToDictionary(x => x.projectName, x => x.taskCunt);
        }

        public static List<MyTask> GetTasksByUser(int Id)
        {
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .Where(t => t.PerformerId == Id && t.Name.Length < 45)
               .ToList();
        }
        public static List<MyTaskInfo> GetFinishedTasksByUser(int Id)
        {
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .Where(t => t.PerformerId == Id && t.FinishedAt != null)
               .Select(t => new MyTaskInfo { Id = t.Id, Name = t.Name }).ToList();
        }

        public static IEnumerable<IGrouping<int?, User>> GetUsersFromTeam()
        {
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .Select(u => u.Performer)
               .Where(u => (DateTime.Now.Year - u.BirthDay.Year) > 10)
               .Distinct().OrderBy(y => y.RegisteredAt)
               .GroupBy(t => t.TeamId);
        }

        public static List<User> GetUsersByName()
        {
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .OrderByDescending(t => t.Name)
               .Select(u => u.Performer)
               .Distinct()
               .OrderBy(y => y.FirstName)
               .ToList();
        }

        public static UserInfo GetUserInfo(int Id)
        {
            return _hierarchy
               .Select(p => new UserInfo
               {
                   User = _hierarchy
                           .SelectMany(t => t.Tasks)
                           .Select(u => u.Performer)
                           .Where(u => u.Id == Id)
                           .Select(u => u)
                           .First(),
                   LastProject = _hierarchy
                            .Where(u => u.AuthorId == Id)
                            .OrderBy(x => x.CreatedAt)
                            .First(),
                   AllTasks = _hierarchy
                            .Where(u => u.AuthorId == Id)
                            .OrderBy(x => x.CreatedAt)
                            .First().Tasks
                            .Count(),
                   AllUnfinishedTasks = _hierarchy
                           .SelectMany(t => t.Tasks)
                           .Where(t => t.PerformerId == Id && t.FinishedAt == null)
                           .Count(),
                   LongestTask = _hierarchy
                           .SelectMany(t => t.Tasks)
                           .Where(t => t.PerformerId == Id)
                           .OrderByDescending(x => (x.FinishedAt ?? DateTime.Now) - x.CreatedAt)
                           .First()
               }).ToList()[0];
        }

        public static List<ProjectInfo> GetProjectInfo()
        {
            return _hierarchy
               .Select(p => new ProjectInfo
               {
                   Project = p,
                   LongestTask = p.Tasks
                       .OrderByDescending(x => x.Description.Length)
                       .FirstOrDefault(),
                   ShortestTask = p.Tasks
                        .OrderBy(x => x.Name.Length)
                        .FirstOrDefault(),
                   AllUsersOnProject = p.Tasks
                        .Where(t => p.Description.Length > 20 || p.Tasks.Count() < 3)
                        .Select(t => t.Performer)
                        .Count()
               }).ToList();
        }
    }
}
