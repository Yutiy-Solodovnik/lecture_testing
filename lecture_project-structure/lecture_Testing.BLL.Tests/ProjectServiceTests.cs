﻿using FakeItEasy;
using lecture_EF.BLL;
using lecture_EF.BLL.Services;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System;
using Xunit;

namespace lecture_Testing.BLL.Tests
{
    public class ProjectServiceTests : IDisposable
    {
        private IQueryBuilder _queryBuilder;
        private ProjectService _projectService;
        private IUnitOfWork _unitOfWork;

        public ProjectServiceTests()
        {
            _unitOfWork = A.Fake<IUnitOfWork>();
            _projectService = new ProjectService(_unitOfWork);
            _queryBuilder = A.Fake<IQueryBuilder>();
            _projectService.QueryBuilder = _queryBuilder;}

        [Fact]
        public void Get_Project_Info()
        {
            _projectService.GetProjectInfo();
            A.CallTo(() => _projectService.QueryBuilder.GetProjectInfo()).MustHaveHappenedOnceExactly();
        }
       
        public void Dispose()
        {
            _projectService.Dispose();
        }
    }
}
