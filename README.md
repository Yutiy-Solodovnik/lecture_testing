## Requirements:
Create additional projects for unit and integration tests.   
Write 15-20 unit tests for 10 Business logic queries:   
-7 requests from previous assignments   
-user creation   
-changing a task as completed   
-adding a member to the team    
### Write 10 integration tests for 5 API requests (the whole request, including working with the database):   
-Project creation   
-Deleting a user   
-Team building   
-One of the requests from the previous job   
-Deleting a task   
-Add new functionality - API endpoint, which returns a selection of all outstanding tasks for the user (from all projects) and cover it with tests.    
# P.S. [Path to json files that contain data for seeding DB, are listed globally]